# Rwrdme-front-end

Rwrdme-front-end repository includes React based front-end of web service aimed for individuals to keep up their daily habits.

### WIP Demo:[backstreet-boyz.gitlab.io/rwrdme-front-end](https://backstreet-boyz.gitlab.io/rwrdme-front-end)


#### Backend repository
- [Rwrdme-back-end](https://gitlab.com/backstreet-boyz/rwrdme)

## Front-end

- **[React](https://facebook.github.io/react/)** and **[Redux](http://redux.js.org/)**
- **[Semantic-ui-react](http://react.semantic-ui.com/)** - UI components.
- **[React-Router v4](https://github.com/ReactTraining/react-router)** 
- **[Redux-form](https://github.com/erikras/redux-form)**
- **[Redux-thunk](https://github.com/gaearon/redux-thunk)**,  **[Redux-Devtools-Extension](https://github.com/zalmoxisus/redux-devtools-extension)**
- **[Redux-promise-middleware](https://github.com/pburtchaell/redux-promise-middleware)**

### Testing
- **[Jest](https://facebook.github.io/jest/)** 
- **[Enzyme](https://github.com/airbnb/enzyme)** 


### Continuous integration
- Gitlab CI deploying to Gitlab pages [docs](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/)

#### Create React App Documentation

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Refer Create React App guide how to perform common tasks.<br>
You can find the most recent version of guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

