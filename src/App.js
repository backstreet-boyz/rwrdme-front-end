import React, { Component } from 'react';
import { NavLink, Route } from 'react-router-dom';
import { Container, Menu } from 'semantic-ui-react';
import HabitListPage from './pages/habit-list-page/habit-list-page';
import HabitFormPage from './pages/habit-form-page/habit-form-page';

class App extends Component {
  render() {
    return (
      <Container>
          <Menu fixed="top">
            <Menu.Item header>Rwrdme</Menu.Item>
            <Menu.Menu>
              <NavLink className="item" activeClassName="active" exact to="/">
                Habits List
              </NavLink>
              <NavLink className="item" activeClassName="active" exact to="/habits/new">
                Add Habit
              </NavLink>
            </Menu.Menu>
          </Menu>
        <Container style={{ padding: '5em 0em' }}>  
          <Route exact path="/" component={HabitListPage}/>
          <Route path="/habits/new" component={HabitFormPage}/>
          <Route path="/habits/edit/:id" component={HabitFormPage} />
        </Container> 
      </Container>  
    );
  }
}

export default App;