import React from 'react';
import {render} from 'react-dom';
import App from './App';
import { MemoryRouter } from 'react-router-dom';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';


it('renders without crashing', () => {
  const mockStore = configureMockStore();  
  const div = document.createElement('div');
  shallow(
  <Provider store={mockStore({})}>
    <MemoryRouter>
      <App/>
    </MemoryRouter>
  </Provider>
  , div, () => {});
});
