import { client } from "./";

const url = "/habits";

export function fetchHabits() {
  return dispatch => {
    dispatch({
      type: "FETCH_HABITS",
      payload: client.get(url)
    });
  };
}

export function newHabit() {
  return dispatch => {
    dispatch({
      type: "NEW_HABIT"
    });
  };
}

export function saveHabit(habit) {
  return dispatch => {
    return dispatch({
      type: "SAVE_HABIT",
      payload: client.post(url, habit)
    });
  };
}

export const fetchHabit = id => (dispatch, getState) => {
  return dispatch({
    type: "FETCH_HABIT",
    payload: client.get(`${url}/${id}`)
  });
};

export const updateHabit = habit => (dispatch, getState) => {
  return dispatch({
    type: "UPDATE_HABIT",
    payload: client.put(`${url}/${habit.id}`, habit)
  });
};

export const deleteHabit = id => (dispatch, getState) => {
  return dispatch({
    type: "DELETE_HABIT",
    payload: client.delete(`${url}/${id}`)
  });
};

export const finishTimer = id => (dispatch, getState) =>  {
   return dispatch({
      type: "FINISH_TIMER",
      id
    });
}
