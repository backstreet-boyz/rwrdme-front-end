import React from "react";
import { Card, Button, Icon, Label } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Divider } from "semantic-ui-react";
import Toggle from "../toggle/toggle";
import PropTypes from 'prop-types';

export const HabitCard = ({
  habit,
  deleteHabit,
  updateHabit,
  show,
  toggleElement
}) => {
  return (
    <Card>
      <Card.Content>
        <Icon
          link
          name="setting"
          className="right floated"
          onClick={() => toggleElement()}
        />
        <Card.Header>
          {habit.habitName}
        </Card.Header>
        <Card.Description>
         <Divider hidden/>
         <Label basic> Duration: <Label.Detail>{habit.duration} minutes</Label.Detail></Label>  
         <Label basic> Cue: {habit.cue} </Label> 
        
        </Card.Description>
        <Card.Description />
      </Card.Content>

      <Card.Content extra>
        {show
          ? <div className="ui two buttons">
              <Link
                to={`/habits/edit/${habit.id}`}
                className="ui basic button green"
              >
                Edit
              </Link>
              <Button basic color="red" onClick={() => deleteHabit(habit.id)}>
                Delete
              </Button>
            </div>
          : <Toggle habit={habit} updateHabit={updateHabit} />}
      </Card.Content>
    </Card>
  );
};

HabitCard.propTypes = {
  habit: PropTypes.object.isRequired
};

export default HabitCard;
