import React from "react";
import { shallow, mount } from "enzyme";
import HabitCard from "./habit-card";
import { Card, LabelDetail } from "semantic-ui-react";

import serializer from "jest-serializer-enzyme";
// import { habits } from '../../habit-data';

expect.addSnapshotSerializer(serializer);

const setup = propOverrides => {
  const props = Object.assign(
    {
      habit: {
        createdAt: "2017-08-07T07:14:08.804Z",
        cue: "Morning",
        duration: 5,
        habitName: "Exercise",
        id: "13",
        lastCompleted: "2017-09-20T04:35:22.545Z",
        reward: null,
        timesDoneSuccessively: null,
        updatedAt: "2017-08-07T07:14:08.804Z"
      }
    },
    propOverrides
  );
  const wrapper = shallow(<HabitCard {...props} />);

  return {
    props,
    wrapper
  };
};

describe("When given habit", () => {
  
   const { wrapper } = setup();
    
  it("should render correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("should render the habit header", () => {
    const cardHeader = wrapper.find('CardHeader');    
    expect(cardHeader.length).toEqual(1);
  });

  it("should render the habit header's text", () => {
    const cardHeader = wrapper.find('CardHeader');    
    expect(cardHeader.contains('Exercise')).toBeTruthy() ;
  });

  it("should render the duration", () => {
    const cardDescription = wrapper.find('CardDescription');
    const labelDetail = cardDescription.find('LabelDetail').shallow(); 
    expect(labelDetail.text()).toEqual('5 minutes');
  });

});

