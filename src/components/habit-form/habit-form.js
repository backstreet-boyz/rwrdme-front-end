import React, { Component } from "react";
import { Form, Grid, Button } from "semantic-ui-react";
import { Field, reduxForm } from "redux-form";
import classnames from "classnames";

class HabitForm extends Component {
  componentWillReceiveProps(nextProps) {
   const { habit } = nextProps;
   if (habit.id !== this.props.habit.id){
     this.props.initialize(habit);
   } 
  }

  renderField = ({ input, label, type, meta: { touched, error } }) =>
    <Form.Field className={classnames({ error: touched && error })}>
      <label>
        {label}
      </label>
      <input {...input} placeholder={label} type={type} />
      {touched &&
        error &&
        <span className="error">
          {error.message}
        </span>}
    </Form.Field>

  render() {
    const { handleSubmit, pristine, submitting, loading } = this.props;

    return (
      <Grid centered columns={2}>
        <Grid.Column>
          <h1 style={{marginTop:"1em"}}>{this.props.habit.id ? 'Edit Habit' : 'Add New Habit'}</h1>
          <Form onSubmit={handleSubmit} loading={loading}>
            <Field
              name="habitName"
              type="text"
              component={this.renderField}
              label="Name of the habit"
            />
            <Form.Group widths="3">
              <Field
                name="duration"
                type="number"
                parse={value => Number(value)}
                component={this.renderField}
                label="Duration in minutes"
              />
            </Form.Group>

             <Field
              name="cue"
              type="text"
              component={this.renderField}
              label="Cue for the habit"
            />
            
            <Button primary type="submit" disabled={pristine || submitting}>
              Save
            </Button>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

const validate = (values) => {
  const errors = {name:{}};
  if(!values.habitName ) {
    errors.habitName = {
      message: 'You need to provide the name'
    }
  }

  if(!values.duration) {
    errors.duration = {
      message: 'You need to provide a duration for the habit.'
    }
  }

  if(!values.cue) {
    errors.cue = {
      message: 'You need to provide a cue for the habit.'
    }
  }

  return errors;
}

export default reduxForm({ form: "habit", validate })(HabitForm);
