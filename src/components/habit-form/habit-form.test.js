import React from 'react';
import { shallow } from 'enzyme';
import HabitForm from "./habit-form";
import serializer from "jest-serializer-enzyme";
// import { habits } from '../../habit-data';

expect.addSnapshotSerializer(serializer);

const setup = propOverrides => {
    console.log(propOverrides);

    const props = Object.assign({
      habit: {},
      loading: false
    }, propOverrides)
    const wrapper = shallow(<HabitForm {...props} />)
  
    return {
      props,
      wrapper
    }
}

describe('When creating new habit', () => {
    it('should render correctly', () => {
        const { wrapper } = setup();
        expect(wrapper).toMatchSnapshot();
    });
});