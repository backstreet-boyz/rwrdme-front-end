import React from "react";
import { Card } from "semantic-ui-react";
import HabitCard from "../habit-card/habit-card";

export default function HabitList({
  habits,
  deleteHabit,
  updateHabit,
  show,
  toggleElement
}) {
  const list = () => {
    return habits.map(habit => {
      return (
        <HabitCard
          key={habit.id}
          habit={habit}
          deleteHabit={deleteHabit}
          updateHabit={updateHabit}
          show={show}
          toggleElement={toggleElement}
        />
      );
    });
  };

  return (
    <Card.Group itemsPerRow={3}>
      {list()}
    </Card.Group>
  );
}
