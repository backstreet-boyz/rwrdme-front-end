import React from 'react';
import { shallow } from 'enzyme';
import HabitList from "./habit-list";
import serializer from "jest-serializer-enzyme";
import { habits } from '../../habit-data';

expect.addSnapshotSerializer(serializer);

const setup = propOverrides => {
    const props = Object.assign({
      habits: habits
    }, propOverrides)
  
    const wrapper = shallow(<HabitList {...props} />)
  
    return {
      props,
      wrapper
    }
  }


describe('HabitList', () => {
    let wrapper;
      
    describe('When given empty habits', () => {
        it('should render empty Card.Group', () => {
            const habits = [];
            wrapper = shallow(
                <HabitList habits={habits} />
            );
            expect(wrapper).toMatchSnapshot();
        });
    });

    describe('When given some habits', () => {
        it('should render the HabitCards', () => {
            const { wrapper } = setup();
            expect(wrapper).toMatchSnapshot();
        });
    });

    
});
