import React, { Component } from "react";
import moment from "moment";
import "moment-duration-format";
import { Segment } from "semantic-ui-react";

class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seconds: this.transformMinutesToSeconds(this.props.start),
      time: 0
    };
  }

  componentDidMount() {
    const { isButtonPressed } = this.props;
    if (isButtonPressed === true && !this.timer) {
      this.transformMinutesToSeconds(this.state.seconds);
      this.tick();
    }
  }
  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  transformMinutesToSeconds(minutes) {
    if (minutes) {
      return minutes * 60;
    } else {
      return 0;
    }
  }

  tick = () => {
    const nextSecond = 1000 - Date.now() % 1000;
    this.timer = setTimeout(this.tick, nextSecond);
    console.log(this.state);

    let seconds = this.state.seconds - 1;
    this.setState({
      seconds: seconds,
      time: moment.duration(seconds, "seconds").format("mm:ss", { trim: false })
    });

    if (seconds <= 0) {
      clearTimeout(this.timer);
      this.props.finishTimer();
    }
  };
  render() {
    return (
      <Segment inverted color="green" textAlign="center" size="tiny">
        <b>
          {this.state.time}
        </b>
      </Segment>
    );
  }
}

export default Timer;
