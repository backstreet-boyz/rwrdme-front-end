import React, { Component } from "react";
import { Button } from "semantic-ui-react";
import TimerContainer from "../../pages/timer-container";
import moment from "moment";

class Toggle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      content: "Start",
      isButtonPressed: false
    };
  }

  componentDidMount() {
    if (this.dateIsSame(this.props.habit.lastCompleted)) {
      this.setState({
        active: true,
        content: "Completed"
      });
    }
  }

  handleClick = habit => {
    this.setState({
      active: true,
      isButtonPressed: true,
      content: "Completed"
    });

    if (!this.dateIsSame(habit.lastCompleted)) {
      const habitWithDate = { ...habit, lastCompleted: moment().toISOString() };
      this.props.updateHabit(habitWithDate);
    }
  };

  dateIsSame = date => {
    const dateIsSame = moment().isSame(date, "day");
    return dateIsSame;
  };

  render() {
    const { active } = this.state;
    const { isButtonPressed } = this.state;
    const { content } = this.state;
    const { habit } = this.props;
    const isButtonVisible = isButtonPressed && !habit.timerFinished;

    return (
      <div>
        {!isButtonVisible &&
          <Button
            fluid
            color={active ? "green" : null}
            active={active}
            onClick={() => this.handleClick(habit)}
            content={content}
          />}

        {isButtonVisible &&
          <TimerContainer
            habitId={habit.id}
            start={habit.duration}
            isButtonPressed={isButtonPressed}
          />}
      </div>
    );
  }
}

export default Toggle;
