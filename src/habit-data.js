export const habits = [
  {
    habitName: 'Exercise',
    timesDoneSuccessively: null,
    cue: 'Morning',
    duration: 5,
    reward: null,
    lastCompleted: '2017-08-24T05:23:25.193Z',
    createdAt: '2017-08-07T07:14:08.804Z',
    updatedAt: '2017-08-07T07:14:08.804Z',
    id: '13',
    timerFinished: true
  },
  {
    id: '14',
    habitName: 'Reading',
    timesDoneSuccessively: null,
    cue: 'Morning',
    duration: 5,
    reward: null,
    lastCompleted: '2017-08-24T04:33:15.135Z',
    createdAt: '2017-08-07T07:14:08.804Z',
    updatedAt: '2017-08-07T07:14:08.804Z'
  },
  {
    id: '15',
    habitName: 'Meditation',
    timesDoneSuccessively: null,
    cue: 'Morning',
    duration: 5,
    reward: null,
    lastCompleted: '2017-08-24T04:49:37.289Z',
    createdAt: '2017-08-07T07:14:08.804Z',
    updatedAt: '2017-08-07T07:14:08.804Z'
  }
];