import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import App from './App';
import store from './store';
import registerServiceWorker from './registerServiceWorker';
import 'semantic-ui-css/semantic.min.css';
import './index.css';


ReactDOM.render(
<BrowserRouter basename="/rwrdme-front-end">   
    <Provider store={store}>
        <App />
    </Provider>
</BrowserRouter>, 
document.getElementById('root'));
registerServiceWorker();
