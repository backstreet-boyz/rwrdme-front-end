import React, { Component } from "react";
import { Redirect } from "react-router";
import { SubmissionError } from "redux-form";
import { connect } from "react-redux";
import { newHabit, saveHabit, fetchHabit, updateHabit } from "../../actions/habit-actions";
import HabitForm from "../../components/habit-form/habit-form";

class HabitFormPage extends Component {
  state = {
    redirect: false
  };
  componentDidMount = () => {
    const { id } = this.props.match.params;
    if (id){
      this.props.fetchHabit(id);
    } else {
      this.props.newHabit();
    }
  }

  submit = (habit) => {
    if(!habit.id) {
     return this.props.saveHabit(habit)
        .then(response => this.setState({redirect:true}))
        .catch(err =>{
            throw new SubmissionError(this.props.errors)
        })
    } else{
      return this.props.updateHabit(habit)
        .then(response => this.setState({redirect:true}))
        .catch(err =>{
            throw new SubmissionError(this.props.errors)
        })
    }     
  }

  render() {
    return (
      <div>
        {this.state.redirect
          ? <Redirect to="/" />
          : <HabitForm habit={this.props.habit} loading={this.props.loading} onSubmit={this.submit} />}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    habit: state.habitStore.habit,
    errors: state.habitStore.errors
  };
};

export default connect(mapStateToProps, {newHabit, saveHabit, fetchHabit, updateHabit})(HabitFormPage) ;
