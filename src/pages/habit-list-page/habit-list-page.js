import React, { Component } from "react";
import { connect } from "react-redux";
import HabitList from "../../components/habit-list/habit-list";
import { Divider, Message, Loader } from "semantic-ui-react";
import Emoji from 'react-emoji-render';
import {
  fetchHabits,
  deleteHabit,
  updateHabit
} from "../../actions/habit-actions";

class HabitListPage extends Component {
  
  constructor(props) {
    super(props);
    this.state = { show: false };
    this.toggleElement = this.toggleElement.bind(this);
  }

  toggleElement() {
    this.setState({
      show: !this.state.show
    });
  }

  componentDidMount() {
    this.props.fetchHabits();
  }

  render() {
    const {loading} = this.props;
    if(loading) {
      return <Loader active>Hold on tight. Unfreezing the instance can take some time.</Loader>;
    }
    return (
      <div>     
        <Divider hidden />
        <h1>Habits</h1>
        <Divider hidden />
        <HabitList
          habits={this.props.habits}
          deleteHabit={this.props.deleteHabit}
          updateHabit={this.props.updateHabit}
          show={this.state.show}
          toggleElement={this.toggleElement}
        />
        <Divider hidden />
        <Message>
        <Emoji text="☝ " />
             Remember your motivation and reason you are doing these habits: 
            <b> To live longer and healthier life.</b>
        </Message>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    habits: state.habitStore.habits,
    loading: state.habitStore.loading    
  };
}

export default connect(mapStateToProps, {
  fetchHabits,
  deleteHabit,
  updateHabit
})(HabitListPage);
