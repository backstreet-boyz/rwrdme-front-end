import Timer from "../components/timer/timer";
import { connect } from "react-redux";
import { finishTimer } from "../actions/habit-actions";

const mapStateToProps = (state, { isButtonPressed, start }) => ({
    isButtonPressed,
    start
});

const mapDispatchToProps = (dispatch, { habitId }) => ({
    finishTimer: () => dispatch(finishTimer(habitId)) // Pass ALL smart stuff in
});

export default connect(mapStateToProps, mapDispatchToProps)(Timer);
