const defaultState = {
  habits: [],
  habit: { habitName: "" },
  loading: false,
  errors: {}
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case "FETCH_HABITS": {
      return {
        ...state,
        habits: action.payload.data.data || action.payload.data
      };
    }
    case "FETCH_HABITS_PENDING": {
      return {
        ...state,
        loading: true,
        habit: { habitName: "" }
      };
    }
    case "FETCH_HABITS_FULFILLED": {
      return {
        ...state,
        loading: false,
        habits: action.payload.data.data || action.payload.data
      };
    }
    case "NEW_HABIT": {
      return {
        ...state,
        habit: { habitName: "" }
      };
    }
    case "SAVE_HABIT_PENDING": {
      return {
        ...state,
        loading: true
      };
    }
    case "SAVE_HABIT_FULFILLED": {
      return {
        ...state,
        habits: [...state.habits, action.payload.data],
        errors: {},
        loading: false
      };
    }
    case "SAVE_HABIT_REJECTED": {
      const data = action.payload.response.data;
      const { habitName } = data.errors;
      const errors = { global: data.message, habitName };
      return {
        ...state,
        errors: errors,
        loading: false
      };
    }

    case "UPDATE_HABIT_PENDING": {
      return {
        ...state,
        loading: false
      };
    }

    case "UPDATE_HABIT_FULFILLED": {
      const habit = action.payload.data;
      return {
        ...state,
        habits: state.habits.map(item => (item.id === habit.id ? habit : item)),
        errors: {},
        loading: false
      };
    }

    case "UPDATE_HABIT_REJECTED": {
      const data = action.payload.response.data;
      const { habitName } = data.errors;
      const errors = { global: data.message, habitName };
      return {
        ...state,
        errors: errors,
        loading: false
      };
    }

    case "DELETE_HABIT_FULFILLED": {
      const id = action.payload.data.id;
      return {
        ...state,
        habits: state.habits.filter(item => item.id !== id)
      };
    }

    case "FINISH_TIMER": {
      const id = action.id;
      console.log(id);
      return {
        ...state,
        habits: state.habits.map(
          item => (item.id === id ? { ...item, timerFinished: true } : item)
        )
      };
    }

    default:
      return state;
  }
};
