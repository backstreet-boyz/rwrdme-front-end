import { combineReducers } from "redux";
import HabitReducer from "./habit-reducer";
import { reducer as formReducer } from "redux-form";

const reducers = {
  habitStore: HabitReducer,
  form: formReducer
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
